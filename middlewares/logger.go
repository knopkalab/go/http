package middlewares

import (
	"fmt"

	"gitlab.com/knopkalab/go/errors"
	"gitlab.com/knopkalab/tracer"

	"gitlab.com/knopkalab/go/http"
)

type Logger struct {
	RequestInfo bool
}

func (logger *Logger) Handler(next http.Handler) http.Handler {
	return func(r *http.Request) (err error) {
		requestRoute := r.RoutePath
		if len(requestRoute) == 0 {
			requestRoute = "404"
		}

		ctx, span := tracer.Start(r.Context, "http_request")
		defer span.Finish()
		r.Context = ctx

		if logger.RequestInfo {
			span.Debug().
				Str("uri", r.URI()).
				Str("method", r.Method).
				Str("route", requestRoute).
				Str("remote_addr", r.RemoteAddr).
				Str("real_ip", r.RealIP()).
				Int64("content_length", r.ContentLength).
				Msg("request info")
		}

		defer func() {
			var event *tracer.Event

			if rec := recover(); rec != nil {
				if err, _ = rec.(error); err == nil {
					err = errors.New(fmt.Sprint(rec)).Skip(2)
				} else if !errors.Is(err) {
					err = errors.Stack(err).Skip(2)
				}
				event = span.Panic()
			} else if err == nil {
				event = span.Debug()
			} else if errors.Is(err) {
				event = span.Error(nil)
			} else {
				event = span.Warn()
			}

			if err != nil {
				r.Response.Status(http.StatusInternalServerError)
			}

			status := r.Response.ResponseCode()
			event.
				Stack().Err(err).
				Int("response_code", status).
				Int("response_size", r.Response.ResponseSize()).
				Msgf("%s %d %s", r.Method, status, r.URI())
		}()

		return next(r)
	}
}
