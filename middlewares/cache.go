package middlewares

import (
	"bytes"
	"sync"
	"time"

	"gitlab.com/knopkalab/go/http"
)

// Cache middleware for "GET 200"
type Cache struct {
	MaxAge time.Duration              // 0 - unlimit
	Key    func(*http.Request) string // optional

	kv map[string]*cachedResponse
}

// InvalidateAll cache
func (cache *Cache) InvalidateAll() {
	cache.kv = make(map[string]*cachedResponse)
}

// InvalidateByKey cache
func (cache *Cache) InvalidateByKey(key string) {
	delete(cache.kv, key)
}

func (cache *Cache) Handler(next http.Handler) http.Handler {
	if cache.Key == nil {
		cache.Key = func(r *http.Request) string { return r.URI() }
	}
	cache.kv = make(map[string]*cachedResponse)

	pool := &sync.Pool{New: func() interface{} { return new(cachedResponse) }}

	return func(r *http.Request) error {
		if r.Method != http.MethodGet {
			return next(r)
		}

		key := cache.Key(r)
		value, ok := cache.kv[key]

		if ok {
			if cache.MaxAge == 0 || value.time.After(time.Now()) {
				h := r.Response.Header()
				for k, v := range value.header {
					h[k] = v
				}
				r.Response.WriteHeader(value.code)
				_, err := r.Response.Write(value.body.Bytes())
				return err
			}
			pool.Put(value)
		}

		value = pool.Get().(*cachedResponse)
		value.body.Reset()
		value.header = make(http.Header)

		value.w = r.Response.ResponseWriter
		r.Response.ResponseWriter = value
		err := next(r)
		r.Response.ResponseWriter = value.w
		value.w = nil

		if err == nil && value.code == http.StatusOK {
			value.time = time.Now().Add(cache.MaxAge)
			cache.kv[key] = value
		} else {
			pool.Put(value)
		}
		return err
	}
}

type cachedResponse struct {
	time   time.Time
	code   int
	body   bytes.Buffer
	header http.Header

	w http.ResponseWriter
}

func (c *cachedResponse) Header() http.Header {
	return c.header
}

func (c *cachedResponse) WriteHeader(statusCode int) {
	c.code = statusCode
	c.w.WriteHeader(statusCode)
}

func (c *cachedResponse) Write(data []byte) (int, error) {
	c.body.Write(data)
	return c.w.Write(data)
}
