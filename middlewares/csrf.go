package middlewares

import (
	"sync"

	"gitlab.com/knopkalab/go/utils"

	"gitlab.com/knopkalab/go/http"
)

type CSRF struct {
	TokenLength    int    // default: 32
	TokenLookup    string // default: X-CSRF-Token (header/form field)
	AllowForm      bool
	CookieName     string // default: _csrf
	CookieDomain   string
	CookiePath     string // default: /
	CookieMaxAge   int    // default: 86400 (1 day)
	CookieSecure   bool
	CookieSameSite http.SameSite
	CookieHTTPOnly bool
}

func (csrf *CSRF) Handler(next http.Handler) http.Handler {
	if csrf.CookiePath == "" {
		csrf.CookiePath = "/"
	}
	if csrf.CookieName == "" {
		csrf.CookieName = "_csrf"
	}
	if csrf.TokenLookup == "" {
		csrf.TokenLookup = http.HeaderXCSRFToken
	}
	if csrf.TokenLength == 0 {
		csrf.TokenLength = 32
	}
	if csrf.CookieMaxAge == 0 {
		csrf.CookieMaxAge = 86400 // 60 sec * 60 min * 24 hours = 1 day
	}

	csrfCookiePool := &sync.Pool{New: func() interface{} {
		return &http.Cookie{
			Domain:   csrf.CookieDomain,
			Path:     csrf.CookiePath,
			Name:     csrf.CookieName,
			MaxAge:   csrf.CookieMaxAge,
			Secure:   csrf.CookieSecure,
			SameSite: csrf.CookieSameSite,
			HttpOnly: csrf.CookieHTTPOnly,
		}
	}}

	return func(r *http.Request) error {
		var token string
		cookie, err := r.Cookie(csrf.CookieName)
		if err == nil {
			token = cookie.Value
		} else {
			token = utils.RandString(csrf.TokenLength)
		}

		switch r.Method {
		case http.MethodGet, http.MethodHead, http.MethodOptions, http.MethodTrace:
		default:
			csrfHeader := r.Header.Get(csrf.TokenLookup)
			if csrfHeader == "" && csrf.AllowForm {
				csrfHeader = r.FormValue(csrf.TokenLookup)
			}
			if csrfHeader != "" && !utils.ConstantTimeCompareStrings(csrfHeader, token) {
				return r.Response.String(http.StatusForbidden, "invalid csrf token")
			}
		}

		r.Response.Header().Set(csrf.TokenLookup, token)
		cookie = csrfCookiePool.Get().(*http.Cookie)
		cookie.Value = token
		r.Response.SetCookie(cookie)
		csrfCookiePool.Put(cookie)

		return next(r)
	}
}
