package middlewares

import "gitlab.com/knopkalab/go/http"

type BasicAuth struct {
	Resolve func(username, password string) bool
}

func (auth *BasicAuth) Handler(nextHandler http.Handler) http.Handler {
	if auth.Resolve == nil {
		panic("resolve func no set")
	}
	return func(r *http.Request) error {
		r.Response.Header().Set(http.HeaderWWWAuthenticate, `Basic realm="Restricted"`)

		user, pass, ok := r.BasicAuth()
		if !ok || !auth.Resolve(user, pass) {
			return r.Response.StatusUnauthorized()
		}

		return nextHandler(r)
	}
}
