package http

import (
	"encoding/json"
	"io"
	"net/http/httptest"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRouter(t *testing.T) {
	r := NewRouter().WithLogNewRoutes(func(method, path string) {
		println("route", method, path)
	})

	var logString string

	w1 := func(next Handler) Handler {
		return func(r *Request) error {
			logString += "w1 "
			return next(r)
		}
	}
	w2 := func(next Handler) Handler {
		return func(r *Request) error {
			logString += "w2 "
			return next(r)
		}
	}

	v1 := func(r *Request) error {
		return r.Response.String(200, "write ok")
	}
	v2 := func(r *Request) error {
		resp := r.RouteStr("name")
		resp += " " + strconv.FormatInt(r.RouteInt("num"), 10)
		resp += " " + r.RouteTail
		return r.Response.String(200, resp)
	}
	nf := func(r *Request) error {
		return r.Response.String(404, "no no no")
	}

	r.AddMiddleware(MiddlewareFunc(w1))
	r.Group("", func() {
		r.AddMiddleware(MiddlewareFunc(w2))
		r.Group("/api", func() {
			r.GET("/123", v1)
			rName := r.Fork("/123/:name")
			rName.GET("/#num/*", v2)
		})
	})

	r.NotFound(nf)

	respCode, resp := routerRequest(r, "GET", "/api/123", nil)
	assert.Equal(t, StatusOK, respCode)
	assert.Equal(t, "write ok", resp)
	assert.Equal(t, "w1 w2 ", logString)

	logString = ""
	respCode, resp = routerRequest(r, "GET", "/wefewfwefw", nil)
	assert.Equal(t, StatusNotFound, respCode)
	assert.Equal(t, "no no no", resp)
	assert.Equal(t, "w1 ", logString)

	_, resp = routerRequest(r, "GET", "/api/123/name1/123/other", nil)
	assert.Equal(t, "name1 123 other", resp)

	type jsonResponse struct {
		A int
		B string
	}
	r.GET("/json", func(r *Request) error {
		return r.Response.JSON(StatusOK, &jsonResponse{A: 5, B: "6"})
	})
	_, resp = routerRequest(r, "GET", "/json", nil)
	var res jsonResponse
	assert.NoError(t, json.Unmarshal([]byte(resp), &res))
	assert.Equal(t, 5, res.A)
	assert.Equal(t, "6", res.B)
}

func BenchmarkRouter(t *testing.B) {
	r := NewRouter()

	v := func(r *Request) error {
		return r.Response.String(200, "hello world!")
	}

	r.GET("/api/123/:name/#num", v)

	req := httptest.NewRequest("GET", "/api/123/name1/123", nil)
	writer := httptest.NewRecorder()

	t.ResetTimer()
	for i := 0; i < t.N; i++ {
		r.ServeHTTP(writer, req)
	}
}

func routerRequest(r *Router, method, url string, body io.Reader) (code int, resp string) {
	req := httptest.NewRequest(method, url, body)
	writer := httptest.NewRecorder()
	r.ServeHTTP(writer, req)
	return writer.Code, writer.Body.String()
}
