package http

import (
	"mime/multipart"
	"net/http"
)

var (
	// ErrMessageTooLarge is returned by ReadForm if the message form
	// data is too large to be processed.
	ErrMessageTooLarge = multipart.ErrMessageTooLarge

	// ErrNoCookie is returned by Request's Cookie method when a cookie is not found.
	ErrNoCookie = http.ErrNoCookie

	// ErrMissingFile is returned by FormFile when the provided file field name
	// is either not present in the request or not a file field.
	ErrMissingFile = http.ErrMissingFile
)
