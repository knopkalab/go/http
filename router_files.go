package http

import (
	"bytes"
	"embed"
	"io/fs"
	"io/ioutil"
	"mime"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

// File serve
func (r *Router) File(routePath, filename string) {
	r.checkFileRoutePath(routePath)
	r.GET(routePath, func(req *Request) error {
		return r.serveFile(req, filename)
	})
}

// Media fileserver
func (r *Router) Media(routePath, rootDir string) {
	r.checkFileRoutePath(routePath)
	r.GET(routePath, func(req *Request) error {
		if relPath, err := url.PathUnescape(req.RouteTail); err == nil {
			filename := filepath.Join(rootDir, path.Clean("/"+relPath))
			return r.serveFile(req, filename)
		}
		return req.Response.StatusNotFound()
	})
}

func (r *Router) serveFile(req *Request, filename string) error {
	f, err := os.Open(filename)
	if err != nil {
		return req.Response.StatusNotFound()
	}
	defer f.Close()

	fi, err := f.Stat()
	if err != nil || fi.IsDir() {
		return req.Response.StatusNotFound()
	}

	http.ServeContent(req.Response, req.Request, fi.Name(), fi.ModTime(), f)
	return nil
}

// StaticFile serve, compress if set
func (r *Router) StaticFile(routePath, filename string, compress *Compressor) error {
	finfo, err := os.Stat(filename)
	if err != nil {
		return err
	}
	if finfo.IsDir() {
		return http.ErrMissingFile
	}
	if compress != nil {
		if err := compress.Init(); err != nil {
			return err
		}
	}
	file, err := newStaticRouterFile(filename, finfo.ModTime(), compress)
	if err == nil {
		r.GET(routePath, file.Handler)
	}
	return err
}

// StaticFileFS serve, compress if set
func (r *Router) StaticFileFS(routePath, filename string, fs *embed.FS, compress *Compressor) error {
	file, err := fs.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()
	finfo, err := file.Stat()
	if err != nil {
		return err
	}
	if finfo.IsDir() {
		return http.ErrMissingFile
	}
	if compress != nil {
		if err := compress.Init(); err != nil {
			return err
		}
	}
	data, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}
	routeFile, err := newStaticRouterFileData(data, filename, finfo.ModTime(), compress)
	if err == nil {
		r.GET(routePath, routeFile.Handler)
	}
	return err
}

// Static cached fileserver, compress if set
func (r *Router) Static(routePath, rootDir string, compress *Compressor) error {
	r.checkFileRoutePath(routePath)

	if compress != nil {
		if err := compress.Init(); err != nil {
			return err
		}
	}

	dir, err := filepath.Abs(rootDir)
	if err != nil {
		return err
	}
	cache := make(map[string]*routerStaticFile)

	err = filepath.Walk(dir, func(path string, finfo os.FileInfo, err error) error {
		if err == nil && !finfo.IsDir() {
			staticFile, err := newStaticRouterFile(path, finfo.ModTime(), compress)
			if err != nil {
				return err
			}
			path, _ = filepath.Rel(dir, path)
			path = strings.ReplaceAll(path, `\`, `/`)
			cache[path] = staticFile
		}
		return err
	})

	r.GET(routePath, func(r *Request) error {
		if file, ok := cache[r.RouteTail]; ok {
			return file.Handler(r)
		}
		return r.Response.StatusNotFound()
	})
	return err
}

// StaticFS cached fileserver, compress if set
func (r *Router) StaticFS(routePath, rootDir string, fsDir *embed.FS, compress *Compressor) error {
	r.checkFileRoutePath(routePath)

	if compress != nil {
		if err := compress.Init(); err != nil {
			return err
		}
	}

	cache := make(map[string]*routerStaticFile)

	err := fs.WalkDir(fsDir, rootDir, func(path string, entry fs.DirEntry, err error) error {
		if err == nil && !entry.IsDir() {
			finfo, err := entry.Info()
			if err != nil {
				return err
			}
			data, err := fsDir.ReadFile(path)
			if err != nil {
				return err
			}
			staticFile, err := newStaticRouterFileData(data, path, finfo.ModTime(), compress)
			if err != nil {
				return err
			}
			path, _ = filepath.Rel(rootDir, path)
			path = strings.ReplaceAll(path, `\`, `/`)
			cache[path] = staticFile
		}
		return err
	})

	r.GET(routePath, func(r *Request) error {
		if file, ok := cache[r.RouteTail]; ok {
			return file.Handler(r)
		}
		return r.Response.StatusNotFound()
	})
	return err
}

type staticFile struct {
	data    []byte
	size    int
	sizeStr string
}

func newStaticFile(data []byte) *staticFile {
	return &staticFile{
		data:    data,
		size:    len(data),
		sizeStr: strconv.Itoa(len(data)),
	}
}

type routerStaticFile struct {
	modified string
	ctype    string
	raw      *staticFile
	gzip     *staticFile
	brotli   *staticFile
}

func newStaticRouterFile(path string, modified time.Time, compress *Compressor) (*routerStaticFile, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return newStaticRouterFileData(data, path, modified, compress)
}

func newStaticRouterFileData(
	data []byte, name string, modified time.Time, compress *Compressor) (*routerStaticFile, error) {

	routerFile := &routerStaticFile{
		modified: modified.Format(http.TimeFormat),
		ctype:    mime.TypeByExtension(filepath.Ext(name)),
		raw:      newStaticFile(data),
	}
	if compress == nil || !compress.ValidSize(len(data)) {
		return routerFile, nil
	}

	var buf bytes.Buffer
	if !compress.GzipDisable {
		if err := compress.ApplyGzip(&buf, data); err != nil {
			return routerFile, err
		}
		if float64(buf.Len())/float64(len(data)) < compress.GzipMinFactor {
			routerFile.gzip = newStaticFile(buf.Bytes())
		}
	}
	if !compress.BrotliDisable {
		buf.Reset()
		if err := compress.ApplyBrotli(&buf, data); err != nil {
			return routerFile, err
		}
		if float64(buf.Len())/float64(len(data)) < compress.BrotliMinFactor {
			routerFile.brotli = newStaticFile(buf.Bytes())
		}
	}
	return routerFile, nil
}

func (file *routerStaticFile) Handler(r *Request) error {
	header := r.Response.Header()
	if file.ctype != "" {
		header.Set(HeaderContentType, file.ctype)
	}
	header.Set(HeaderLastModified, file.modified)

	if ifModified := r.Header.Get(HeaderIfModifiedSince); ifModified != "" {
		if ifModified == file.modified {
			return r.Response.Status(StatusNotModified)
		}
	}

	if encoding := r.Header.Get(HeaderAcceptEncoding); encoding != "" {
		haveGzip, haveBrotli := false, false
		encodings := strings.Fields(encoding)
		for _, enc := range encodings {
			if enc == "br" || enc == "br," {
				haveBrotli = true
			} else if enc == "gzip" || enc == "gzip," {
				haveGzip = true
			}
		}
		if file.brotli != nil && haveBrotli {
			header.Set(HeaderContentEncoding, "br")
			header.Set(HeaderContentLength, file.brotli.sizeStr)
			return r.Response.Bytes(StatusOK, file.brotli.data)
		}
		if file.gzip != nil && haveGzip {
			header.Set(HeaderContentEncoding, "gzip")
			header.Set(HeaderContentLength, file.gzip.sizeStr)
			return r.Response.Bytes(StatusOK, file.gzip.data)
		}
	}
	header.Set(HeaderContentLength, file.raw.sizeStr)
	return r.Response.Bytes(StatusOK, file.raw.data)
}
