package http

import (
	"fmt"
	"strconv"
)

// RouteValue of web.Router handler
type RouteValue struct {
	*routeNode
	Raw string
	Int int64
}

type routeNode struct {
	Name   string
	IsSlug bool
	IsInt  bool
	IsTail bool

	childs     [256]*routeNode
	childParam *routeNode

	methodHandlers map[string]Handler
	methodPaths    map[string]string
}

func newRouteNode() *routeNode {
	return &routeNode{
		methodHandlers: map[string]Handler{},
		methodPaths:    map[string]string{},
	}
}

func isRouteParamStart(c byte) bool {
	return c == ':' || c == '*' || c == '#'
}

func (node *routeNode) paramSet(payload string) {
	if payload[0] == '*' {
		node.IsTail = true
		return
	}
	node.Name = payload[1:]
	if node.Name == "" {
		panic("empty route param")
	}
	switch payload[0] {
	case ':':
		node.IsSlug = true
	case '#':
		node.IsInt = true
	default:
		panic("wrong parameter")
	}
}

func (node *routeNode) setRouteValue(r *Request, rawValue string) (err error) {
	rValue := RouteValue{routeNode: node, Raw: rawValue}
	if node.IsInt {
		rValue.Int, err = strconv.ParseInt(rawValue, 10, 64)
	}
	r.RouteVals = append(r.RouteVals, rValue)
	return
}

func (node *routeNode) matchRecursiveAndSetRouteValues(r *Request, startPos int) Handler {
	path := r.URL.Path

	var paramRoute *routeNode
	var paramRouteIdx int

	for i := startPos; i < len(path); i++ {
		c := path[i]
		if node.childParam != nil {
			paramRoute = node.childParam
			paramRouteIdx = i
		}
		if child := node.childs[c]; child != nil {
			node = child
			continue
		}
		if paramRoute != nil {
			if paramRoute.IsTail {
				for i = 0; i < len(path) && path[i] != '?'; i++ {
				}
				r.RouteTail = path[paramRouteIdx:i]
				node = paramRoute
				break
			}
			i = paramRouteIdx
			for ; i < len(path) && path[i] != '/'; i++ {
			}
			if paramRoute.setRouteValue(r, path[paramRouteIdx:i]) != nil {
				return nil
			}
			return paramRoute.matchRecursiveAndSetRouteValues(r, i)
		}
		return nil
	}
	r.RoutePath = node.methodPaths[r.Method]
	return node.methodHandlers[r.Method]
}

func (node *routeNode) setRouteHandler(method, path string, h Handler) {
	for i := 0; i < len(path); i++ {
		c := path[i]
		if isRouteParamStart(c) {
			iStart := i
			for ; i < len(path) && path[i] != '/'; i++ {
			}
			if node.childParam == nil {
				node.childParam = newRouteNode()
			}
			node = node.childParam
			node.paramSet(path[iStart:i])
			if node.IsTail {
				break
			}
			i--
		} else {
			child := node.childs[c]
			if child == nil {
				child = newRouteNode()
				node.childs[c] = child
			}
			node = child
		}
	}
	if node.methodHandlers[method] != nil {
		panic(fmt.Sprintf("handler on route [%s] %s already exists", method, path))
	}
	node.methodHandlers[method] = h
	node.methodPaths[method] = path
}
