package http

import (
	"compress/gzip"
	"io"

	"github.com/andybalholm/brotli"
)

// Compressor config for static route
type Compressor struct {
	init bool

	MinSize int // default: 0 bytes
	MaxSize int // default: unlimit

	GzipDisable   bool
	GzipMinFactor float64 // default: 0.75 from origin size
	GzipLevel     int     // range [0,9], default: 9

	BrotliDisable   bool
	BrotliMinFactor float64 // default: 0.75 from origin size
	BrotliLevel     int     // range [0,11], default: 6

	gzip   *gzip.Writer
	brotli *brotli.Writer
}

// Init compressor
func (c *Compressor) Init() error {
	if c.init {
		return nil
	}
	c.init = true

	if c.GzipMinFactor == 0 {
		c.GzipMinFactor = 0.75
	}
	if c.GzipLevel == 0 {
		c.GzipLevel = gzip.BestCompression
	}
	if c.BrotliMinFactor == 0 {
		c.BrotliMinFactor = 0.75
	}
	if c.BrotliLevel == 0 {
		c.BrotliLevel = brotli.DefaultCompression
	}

	c.brotli = brotli.NewWriterLevel(nil, c.BrotliLevel)
	var err error
	c.gzip, err = gzip.NewWriterLevel(nil, c.GzipLevel)
	return err
}

// ValidSize by config
func (c *Compressor) ValidSize(size int) bool {
	return size >= c.MinSize && (c.MaxSize == 0 || size <= c.MaxSize)
}

// ApplyGzip compress
func (c *Compressor) ApplyGzip(w io.Writer, data []byte) error {
	c.gzip.Reset(w)
	if _, err := c.gzip.Write(data); err != nil {
		return err
	}
	return c.gzip.Close()
}

// ApplyBrotli compress
func (c *Compressor) ApplyBrotli(w io.Writer, data []byte) error {
	c.brotli.Reset(w)
	if _, err := c.brotli.Write(data); err != nil {
		return err
	}
	return c.brotli.Close()
}
