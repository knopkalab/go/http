package http

import (
	"net/http"
)

type Handler func(*Request) error

func WrapHandlerFunc(f http.HandlerFunc) Handler {
	return func(r *Request) error {
		f(r.Response, r.Request)
		return nil
	}
}

func WrapHandler(h http.Handler) Handler {
	return WrapHandlerFunc(h.ServeHTTP)
}

type Middleware interface {
	Handler(next Handler) Handler
}

type MiddlewareFunc func(next Handler) Handler

func (f MiddlewareFunc) Handler(next Handler) Handler {
	return f(next)
}

var DefaultNotFound = func(r *Request) error {
	return r.Response.StatusNotFound()
}

type NewRoutesCallback func(method, path string)

func NewRouter() *Router {
	return &Router{
		node:        newRouteNode(),
		middlewares: []Middleware{},
		notFound:    DefaultNotFound,

		newRoutesCallbacks: []NewRoutesCallback{},
	}
}

// Router with handlers and middlewares
type Router struct {
	node        *routeNode
	middlewares []Middleware
	notFound    Handler
	pathPrefix  string

	newRoutesCallbacks []NewRoutesCallback
}

func (r *Router) WithLogNewRoutes(f NewRoutesCallback) *Router {
	r.newRoutesCallbacks = append(r.newRoutesCallbacks, f)
	return r
}

func (r *Router) callLogNewRoutes(method, path string) {
	for _, callback := range r.newRoutesCallbacks {
		callback(method, path)
	}
}

func (r *Router) ServeHTTP(originWriter http.ResponseWriter, originRequest *http.Request) {
	req := GetRequest(originWriter, originRequest)
	defer PutRequest(req)
	if h := r.node.matchRecursiveAndSetRouteValues(req, 0); h != nil {
		h(req)
	} else {
		r.notFound(req)
	}
}

// AddMiddleware for the next handlers
func (r *Router) AddMiddleware(middlewares ...Middleware) {
	for _, m := range middlewares {
		if m == nil {
			continue
		}
		r.middlewares = append(r.middlewares, m)
		r.callLogNewRoutes("M", r.pathPrefix)
	}
}

func (r *Router) wrapHandlerMiddlewares(h Handler) Handler {
	wrapped := h
	for i := len(r.middlewares) - 1; i >= 0; i-- {
		wrapped = r.middlewares[i].Handler(wrapped)
	}
	return wrapped
}

// NotFound set
func (r *Router) NotFound(h Handler) {
	if h == nil {
		return
	}
	r.notFound = r.wrapHandlerMiddlewares(h)
	r.callLogNewRoutes("404", r.pathPrefix)
}

// Group routes by path
func (r *Router) Group(path string, subRoutes func()) {
	p, m := r.pathPrefix, r.middlewares
	r.pathPrefix += path
	subRoutes()
	r.pathPrefix, r.middlewares = p, m
}

// Fork router by path
func (r *Router) Fork(path string) *Router {
	fork := new(Router)
	*fork = *r
	fork.pathPrefix += path
	return fork
}

// ROUTE is add handler to router with http method
func (r *Router) ROUTE(method, path string, h Handler, m ...Middleware) {
	if h == nil {
		return
	}
	r.Group(path, func() {
		r.AddMiddleware(m...)
		r.node.setRouteHandler(method, r.pathPrefix, r.wrapHandlerMiddlewares(h))
		r.callLogNewRoutes(method, r.pathPrefix)
	})
}

// GET is alias ROUTE with method Get
func (r *Router) GET(path string, h Handler, m ...Middleware) {
	r.ROUTE(http.MethodGet, path, h, m...)
}

// HEAD is alias ROUTE with method Head
func (r *Router) HEAD(path string, h Handler, m ...Middleware) {
	r.ROUTE(http.MethodHead, path, h, m...)
}

// POST is alias ROUTE with method Post
func (r *Router) POST(path string, h Handler, m ...Middleware) {
	r.ROUTE(http.MethodPost, path, h, m...)
}

// PUT is alias ROUTE with method Put
func (r *Router) PUT(path string, h Handler, m ...Middleware) {
	r.ROUTE(http.MethodPut, path, h, m...)
}

// PATCH is alias ROUTE with method Patch
func (r *Router) PATCH(path string, h Handler, m ...Middleware) {
	r.ROUTE(http.MethodPatch, path, h, m...)
}

// DELETE is alias ROUTE with method Delete
func (r *Router) DELETE(path string, h Handler, m ...Middleware) {
	r.ROUTE(http.MethodDelete, path, h, m...)
}

// CONNECT is alias ROUTE with method Connect
func (r *Router) CONNECT(path string, h Handler, m ...Middleware) {
	r.ROUTE(http.MethodConnect, path, h, m...)
}

// OPTIONS is alias ROUTE with method Options
func (r *Router) OPTIONS(path string, h Handler, m ...Middleware) {
	r.ROUTE(http.MethodOptions, path, h, m...)
}

// TRACE is alias ROUTE with method Trace
func (r *Router) TRACE(path string, h Handler, m ...Middleware) {
	r.ROUTE(http.MethodTrace, path, h, m...)
}

// Redirect request
func (r *Router) Redirect(path, method, toURL string, permanent bool) {
	if permanent {
		r.ROUTE(method, path, func(r *Request) error {
			return r.Response.RedirectPermanent(toURL)
		})
		return
	}
	r.ROUTE(method, path, func(r *Request) error {
		return r.Response.Redirect(toURL)
	})
}

func (r *Router) checkFileRoutePath(routePath string) {
	path := r.pathPrefix + routePath
	if path[len(path)-1] != '*' {
		panic("incorrect route path ending")
	}
}
